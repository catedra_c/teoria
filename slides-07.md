# Clase 7
---
## Temario
* Tipos de variables
* El modificador `extern`
* El modificador `static`
* El modificador `register`

***
## Tipos de variables
---
## Alcance y tiempo de vida de variables

* **Alcance:** porción del programa donde una variable puede referenciarse por su nombre. En C depende de cómo fue declarada y si se le ha aplicado algún *modificador*

* **Tiempo de vida:** tiempo durante el cual una variable tiene reservada memoria

***
***
## Variables externas

* Las variables pueden definirse:
  * Después de la llave de apertura de un bloque
  * Antes del nombre de una función

```c
#include <stdio.h>

int i = 0;
int main() {
  float p = 9.0;

  printf("%d %f\n", i, p)
  return 0;
}

float z = 7.5;
void print_something(){
  int j= 5;

  printf("%d %f %d\n", i, z, j)
}
```
---
## Variables externas

El alcance abarca desde la definición hasta el final del archivo

```c
 +----- int var_1;
 |      void function_1(int a ){
 |      int aux;
 |      ...
 |      }
 |
 |  +-- int var2_;
 |  |   void function_2(void ){
 |  |    ..
 |  |   }.
 |  |
 |  |  +-int var3_;
 |  |  | ...
 |  |  |
 V  V  V
```
<small class="fragment">
 **¿Cómo se las puede referenciar fuera de ese alcance?**
</small>

---
## El modificador extern

Referencia una variable que no está dentro del archivo fuente o se hace
referencia antes de su definición

```c
int var_1;
void function_1(){
  extern int var_3;
  ...
}
int var_2;
void function_2(){
  ...
}
int var_3;  
```

* Las variables `extern` cumplen que:
  * Se inicializan **solo** en su definición
  * Por defecto se inicializan en 0

<small>
[Descargar ejemplo](images/ejemplos/07/01-extern.zip)
</small>
***
***
## Variables estáticas
---
## Variables estáticas
* Una variable **estática interna** proporciona almacenamiento privado y
  permanente dentro de la función
* Una variable **estática externa** limita su alcance al archivo fuente
  donde está definida
  * Aplica también a funciones

---
## Variables estáticas

```c
#include <stdio.h>

int cuento() {
  static int x;
  return x++;
}

int main() {
int i;
  for(i=0 ; i<10 ; i++) printf("%d\n", cuento());
  return 0;
}
```

* El valor de `x` se mantiene de llamada en llamada
* Se inicializan sólo la primera vez que entra al bloque
* Las variables estáticas se inicializan en 0 por defecto

<small>
[Descargar ejemplo](images/ejemplos/07/02-static.c)
</small>
***
***
## Variables Register
---
## Variables Register

* Si es posible estas variables estarán alocadas en los registros del
  procesador
* Se utilizan en variables que se van a utilizar con mucha frecuencia
* Es una *sugerencia* al compilador
* Tienen valores iniciales indefinidos

```c
  void f(register unsigned m){
    register int i;
    ...
  }
```
***
