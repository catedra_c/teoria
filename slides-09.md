# Clase 9
---
## Temario
* Punteros a funciones
* Punteros a void
* Argumentos variables

***
## Punteros a funciones
---
## Punteros a funciones
* Es uno de los recursos que mayor flexibilidad le otorgan al lenguaje
* Las funciones tienen una ubicación física o dirección que corresponde al segmento de
código donde comienza la función
* Es como un *alias* de la función
  * *Una vez que un puntero apunta a una función se puede invocar a la función a través 
    del puntero*
* En general se utilizan para parametrizarlas funciones como argumentos de otras
* Son punteros a código y no a datos
  * *La dirección de la función se obtiene con el nombre de la misma*
---
## Punteros a funciones

```c

type (* function_name) [(args)]

```

* Los punteros a funciones pueden:
  * Pasar como argumentos
  * Ser retornados por una función
  * Ser miembros de un arreglo
  * Declararse con typedef
---
## Ejemplo

```c
int comprobar(const char *a, const char *b,
              int (*cmp) (const char *, const char *)) {
  return cmp(a,b);
}
```

<small>
[Descargar ejemplo completo](images/ejemplos/09/01-ptr-function.c)
</small>
---
## Typedef y punteros a funciones

```c
typedef double (*ptr_function)(char *);

/* Donde:
 *    * double es el tipo de retorno de la función
 *    * La función recibe un puntero a char
 *    * El nombre del tipo es ptr_function
 */

ptr_function p_func = xxx;
double test = p_func("2.13");
```
<small>
[Descargar ejemplo](images/ejemplos/09/02-ptr-function-typedef.c)
</small>
---
## Arreglos de punteros a funciones

Podemos simplemente definir arreglos de tipo punteros a función:

```c
float (*operations[])(float, float) = { sum, minus, ... }
```

o usar `typedef` para simplificar la lectura:

```c
typedef float (*float_operation_t) (float, float);

float_operation_t operations[] = { sum, minus, ...}
```
<small>
[Descargar ejemplo sin typedef](images/ejemplos/09/03-ptr-function-arr.c)
<br />
[Descargar ejemplo con typedef](images/ejemplos/09/04-ptr-function-arr.c)
</small>
---
## ¿Es necesario usar &/* con punteros a función?
El [estandar](https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html#Calling-Functions-Through-Function-Pointers) dice:

```c
void message (void (*func)(int), int times)
{
  int j;
  for (j=0; j<times; ++j)
    func (j);  /* (*func) (j); would be equivalent. */
}

void example (int want_foo) 
{
  void (*pf)(int) = &bar; /* The & is optional. */
  if (want_foo)
    pf = foo;
  message (pf, 5);
}
```
---
## Uso de qsort

* La librería `stdlib` incluye la implementación del algoritmo de ordenamiento
[Quick Sort](https://es.wikipedia.org/wiki/Quicksort)
  * Ordena cualquier arreglo, siempre que se especifique la forma en que se
    comparan dos elementos de ese arreglo
  * Para ello la función recibe entre sus argumentos, la función de comparación

```c
#include <stdlib.h>

void qsort(void *base, size_t nmemb, size_t size,
          int (*compar)(const void *, const void *));

```

<small>
[Descargar ejemplo](images/ejemplos/09/05-qsort.c)
</small>

***
***
## Punteros genéricos
---
## Pensemos cuándo usarlos...

Desarrollar un algoritmo de manejo de lista o árbol genérico.
Por genérico, entendemos que el mismo algoritmo sea independiente del dato que
almacene


```c
typedef struct node {
  void * data;
  struct node * left;
  struct node * right;
} * t_node;
```
---
## Punteros genéricos

* Un puntero genérico o puntero a `void` es un puntero que puede apuntar a cualquier
tipo de dato.
* Para que un puntero a `void` pueda ser desreferenciado debe **castearse** antes de
ser utilizado.

```c

int main() {
  int var1 = 1;
  double var2 = 1.0;
  void* vptr ;

  vptr = &var1;
  *(int *) vptr = 2;
  vptr = &var2;
  *(float *) vptr = 4.0;
}

```
---
## ¿Cómo sería el arbol genérico?

En la clase 6, vimos una implementación de un árbol binario de búsqueda
implementado con enteros. [Descargar ejemplo](images/ejemplos/06/04-bin-tree.zip)

* La implementación genérica del árbol ahora nos permite:
  * Insertar de forma ordenada tipos difrentes
  * Recorrer el árbol en (in|pre|post)order haciendo lo que necesitemos

[Descargar ejemplo de árbol binario genérico](images/ejemplos/09/06-generic-tree.zip)
***
***
## Lista de argumentos variable
---
## Lista de argumentos variable

Analizando `printf` vemos que podemos usarla de cualquiera de las siguientes
formas:

```c
  printf("Hola Mundo");

  printf("Hola %s", "Mundo");

  printf("%s %s", "Hola", "Mundo");
```

* Observando detalladamente, sabemos que:
  * Un parámetro de los anteriores es **obligatorio**: *el string*
  * En ese mismo argumento, se define con el formato, cuántos argumentos se
    recibirán luego
* El prototipo es

```c
  int printf(const char * format, ...);
```

---
## Uso de argumentos variables

* El soporte lo da la librería `stdagr.h`
* El ejemplo más común: `printf`
* Siempre hay al menos un argumento fijo
* Encabezado general de una función con argumentos variables:

```c

  int my_variable_args(arg1, arg2, argN, ...);

```
---
## ¿Qué provee stdarg?

* Disponemos de las siguientes macros:
  * `va_start`
  * `va_arg`
  * `va_end`
* Además se define el tipo `va_list` que permite declarar una variable que itere
  sobre cada argumento.

```c
  void va_start(va_list ptr, last_arg);
  type va_arg(va_list ptr, type);
  void va_copy(va_list target, va_list source);
  void va_end(va_list ptr);
```
---
## ¿Cómo usar stdarg?
* La función que desee implementar argumentos variables debe tener al menos un
  parámetro previo a la lista variable de argumentos, denominado **last_arg**
* **last_arg** se lo utiliza como segundo argumento en `va_start`
* `va_start` inicializa la lista para acceder a cualquier argumento variable
* `va_arg` se utiliza luego para tomar los argumentos, siendo **type** el tipo del
  siguiente argumento
* Una vez leídos todos los argumentos se llama a `va_end` para restaurar la pila

<small>
[Descargar ejemplo](images/ejemplos/09/07-stdarg.c)
</small>
---
## Variantes de printf

La familia de funciones que se ofrecen por `stdio` considera:

```c
 int vprintf(const char *format, va_list ap);
 int vfprintf(FILE *stream, const char *format, va_list ap);
 int vsprintf(char *str, const char *format, va_list ap);
 int vsnprintf(char *str, size_t size, const char *format, va_list ap);
```

<small>
[Descargar ejemplo](images/ejemplos/09/08-vprintf.c)
</small>

