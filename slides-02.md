# Clase 2
---
## Temario

* Estructuras de control
  * Sentencias compuestas
  * Condicional simple
  * Switch
  * Iteraciones

***
# Estructuras de control
---
## Estructuras de control
* Gobiernan la ejecución de un programa
* Permiten repetir código o saltear algún conjunto de sentencias.


***
***
# Sentencias compuestas
---

## Sentencias compuestas

* También llamadas bloques
* Suelen ser parte de otras estructuras como `if`, `for` , `while`, etc
* Consiste de varias sentencias encerradas entre llaves `{ }`
* No terminan con `;`

```c
{
  int x = 12;
  y = x*2;
}
```

<small>
[Descargar ejemplo](images/ejemplos/02/01-block.c)
</small>

***
***
# Condicional simple
---
## Condicional simple: if
```c
if (condicion) {
  sentencias_verdad
} else {
  sentencias_falso
}
```
### Ejemplo
```
if (x == 3) {
  ...
} else {
  ...
}
```
---
## Buenas prácticas
### En vez de....
```c
if (x != 0) {
...
}

if (x == 0) {
...
}
```

<div class="fragment">
<h3> Usar... </h3>

<pre>
<code class="c">
if (x) {
...
}

if (!x) {
...
}
</code>
</pre>
</div>

***
***
# Condicional compuesto
---
## Condicional compuesto: switch
```c
switch (expresión) {
  case expresion-constante: sentencias
  case expresion-constante: sentencias
  case expresion-constante: sentencias
  default: sentencias
}
```
---
### Ejemplo
```c
...
switch (letra) {
  case 'a': case 'e': case 'i': case 'o': case 'u':
    vocMin++;
    break;
  case 'A': case 'E': case 'I': case 'O': case 'U':
    vocMay++;
    break;
  default:
    otros++;
    break;
}
...
```
<small>
[Descargar ejemplo](images/ejemplos/02/02-switch.c)
<div class="fragment">
El ejemplo lee las palabras como argumentos al programa
</div>
</small>

***
***
# Iteraciones
---
## Iteraciones: while

```c
while (expresión)
  sentencias
```

### Ejemplo

```c
int suma=0,n=10;
while (n--) {
  suma += n;
  ....
}

/* Similar a 
while (n) {
  n = n - 1;
  suma = suma + n;
  ...
*/
```
---
## Iteraciones: do while

```c
do
  sentencias
while (expresión)
```

### Ejemplo

```c
int sumo = 0, n = 10;
do
  sumo += n
while (n--);
```

<small>
[Descargar ejemplo](images/ejemplos/02/03-do-while.c)
<div class="fragment">
El ejemplo retorna el valor calculado. Este valor puede verse con `echo $?`
</div>
</small>

---
## Iteraciones: for

### El for siguiente...
```c
for (expresión1; expresión2; expresión3)
  sentencias;
```
<div class="fragment">
<h3> Equivale a...</h3>
<pre>
<code class="c">
expresión1;
while (expresión2) {
  sentencias;
  expresión3
}
</code>
</pre>
</div>

---
## Iteraciones: for
### Versión 1
```c
int suma=0,n;
for( n=1; n <=10;n++)
  suma+=n;
```

### Versión 2
```c
int suma=0, n=5;
for( ; n <=10;suma+=n, n++);
```
### Versión 3
```c
int suma=0, n=1;
for( ; n <=10; ) {
  suma+=n;
  n++;
}
```
***
