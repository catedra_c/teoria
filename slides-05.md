# Clase 5
---
## Temario
* Punteros
  * Introducción
  * Operadores & y *
  * Pasaje por referencia
  * Arreglos
  * Aritmética de punteros
  * Strings
  * Asignación dinámica
  * Arreglos de punteros

---
# Punteros
---
## Introducción
* Un puntero a un dato es la dirección del primer byte que forma parte del mismo.
* Las variables de tipo puntero contienen la dirección de memoria de un dato.
* Un puntero consta de dos partes:
  * La dirección apuntada
  * El contenido apuntado

**Es importante programarlos en forma correcta y adecuada para evitar
comportamientos inesperados**

***
## Operadores & y *

Para declarar un puntero:

```c

un_tipo * ptr; /* ptr es una variable de tipo puntero a un_tipo.
                  contiene la direccion de memoria de un dato de tipo
                  un_tipo */

```

### Ejemplos

```c

int * var_int;        /* para punteros a enteros */

double * var_double;  /* para punteros a double */

char * var_char;      /* para punteros a char */

```
---
## Operadores & y *

Para desreferenciar una variable usamos `&`

```c
int n=0;  /* n es una variable de tipo int */

int * p;  /* p es una variable de tipo puntero a un int,
             que contendrá la dirección de un int */

p = &n;   /* Se asigna a p el valor de la dirección de n */

```
Para referenciar el contenido de un puntero usamos `*`

* `*p` es equivalente al nombre de la variable `n`
* `*p = 33` es equivalente a `n = 33`
---
## Ejemplo

Supongamos el siguiente extracto de código:

```c

  int x = 5, y = 7, ptr1 = &x, ptr2 = &y;

```

Analizar gráficamente qué sucede con:

* `ptr1 = ptr2`
* `*ptr1 = *ptr2`

***
***
## Pasaje por referencia

Analizando el siguiente código

```c
void cambiar(int a, int b){
  int aux;
  aux = a;
  a = b;
  b = aux;
}

int main (){
  int x = 10, y = 20;
  cambiar(x, y);
}
```

<small class="fragment">
Recordar que en C <strong>no</strong> exite el pasaje por referencia. <br />
El código anterior <strong>no cambia nada</strong>
</small>
---
## Pasaje por referencia

Si recordamos el uso del `scanf`, vemos un ejemplo de cómo se simula el pasaje
por referencia

```c
#include <stdio.h>

int main(){
  int x; char mensaje[30];

  scanf("%d", &x);

  scanf("%s", mensaje);

  return 0;
}
```
<small style="color: red">
Pero.... ¿Mensaje no se precede con &?
</small>
***
***
## Arreglos

* De los arreglos sabemos que:
  * Se definen como `un_tipo variable[TAM];`
  * Se indexan de 0 (cero) a TAM - 1
  * La asignación de valores puede hacerse en el momento de la declaración

### Ejemplos

```c
int x[12];

int num[] = { 1, 2 };

char cadena_1[128] = "Hola Mundo";

char cadena_2[] = "Hola";
```

---

## Arreglos

* El nombre del arreglo referencia a la dirección del elemento 0 del arreglo
* El nombre del arreglo es una constante
* Cuando se declara un arreglo se reservan un número específico de posiciones de
  memoria para ese arreglo
* Son elementos consecutivos en la memoria
* C NO tiene control del índice

<small>
[Descargar ejemplo](images/ejemplos/05/./01-show-array-ptr.c)
</small>

---

## Arreglos como parámetro

* Los arreglos se pasan como parámetro sin especificar la dimensión
* Esto es porque el nombre de un arreglo representa la posición de memoria donde
  se inicia
* La dimensión del arreglo debe pasarse en otro parámetro para controlar acceder
  al arreglo dentro de los límites del mismo


```c

  int sum(int vec[], int dim);

```

---
## Un ejercicio

* Implementar una función que reciba un arreglo de enteros y calcule:
  * La sumatoria
  * El mínimo
  * El máximo
  * El promedio

Y devuelva todos estos valores

<small class="fragment">
[Descargar ejemplo](images/ejemplos/05/02-min-max-avg.c)
</small>
---
### Arreglos multidimensionales

Son arreglos de arreglos

```c
int i, j, matrix[10][20];

for(i = 0; i < 10; i++) {
  for(j = 0; j < 20;j++) {
    matrix[i][j] = 0;
  }
}
```

Inicialización

```c

int matrix_2[3][4] = { { 1, 2, 3, 4 },
                       { 5, 6, 7, 8 },
                       { 9, 10, 11, 12 } };

```

O alternativamente, pero **NO RECOMENDADO**

```c

int matrix_3[3][4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

```
---
## Arreglos multidimensionales como parámetros

```c

void sample(int matrix[][10], int row_size);

```
---
## Punteros y arreglos

Existe una fuerte relación entre punteros y arreglos

**El nombre de un arreglo es una constante puntero**

```c

int my_array[] = {10, 20, 30, 40, 50};

printf("my_array[0] = %d", my_array[0]); /* Imprime: my_array[0] = 10 */

printf("*my_array = %d", *my_array);     /* Imprime: *my_array = 10 */

```

<small class="fragment">
**Todo lo que se logra con indexación de arreglos, se logra con punteros**
</small>

---
## En memoria, el arreglo se almacena

```c

int my_array = { 1, 2 }

```
| Variable    | Posición| Contenido |
|---          |---      |---:       |
| my_array    | 0xA000  | 0XA101    |
| my_array[0] | 0xA101  | 1         |
| my_array[1] | 0xA105  | 2         |

```c

my_array[0] == *my_array

my_array[1] == *(my_array + 1)

```
---
## Punteros y arreglos

Cualquier expresión escrita como arreglo e índice es equivalente a una expresión
escrita como un puntero y un desplazamiento

```c
int my_array[10], *ptr;

/*

  my_array[i] equivale a *(my_array +i)

  &my_array[i] equivale a (my_array +i)

  ptr[i] equivale a *(ptr + i)

*/

```

***
***
## Aritmética de punteros

* Si `p` y `q` apuntan a elementos del **mismo arreglo**, entonces es correcto utilizar
los operadores **==**, **!=**, **<**, **>**, **<=**, **>=**
* ` p < q` es *verdadero* si `p` apunta a un elemento que está antes en el arreglo de lo 
que está el que apunta `q`
* Cualquier puntero se lo puede comparar con `0` (**==** o **!=**)
* Se puede sumar o restar un entero a un puntero:
  * `p + i;`
  * `p - i:`
* Si `p` y `q` apuntan a elementos del mismo arreglo, entonces está permitido la resta
entre `p` y `q`

---
## No es posible

* Sumar punteros
* Multiplicar o dividir punteros
* Enmascarar
* Sumar un float o double a un puntero
* Asignar o copiar un arreglo a otro arreglo

```c

int array_a[] = {1, 2, 3, 4},
    array_b[] = {10, 20, 30, 40};

a = b;  /* NO ES POSIBLE */

a == b  /* Devuelve true si a y b hacen referencia a la misma
         * posición de memoria. No compara elementos 
         */

```
***
***
## Cadenas de caracteres o strings

* El uso más habitual de los arreglos son los strings
* Un string es un arreglo de caracteres terminada en nulo
  * Un nulo es un `0` (cero)
* El compilador completa este `0` (cero) automáticamente.

```c

char cadena[11];

char cadena[]="Hola"; /* Constante string que equivale
                       * a {'H', 'o', 'l', 'a', '\0' }
                       */

```

* Las librerías `string.h` y `strings.h` contienen las funciones
  habituales para la gestión de strings
  * `man string`
---
## Reinventando la rueda

* A continuación veremos ejemplos de funciones de `string.h`
  * [`my_strlen_v1`](images/ejemplos/05/03-my_strlen_v1.c)
  * [`my_strlen_v2`](images/ejemplos/05/04-my_strlen_v2.c)
  * [`my_strlen_v3`](images/ejemplos/05/05-my_strlen_v3.c)
  * [`my_strcpy`](images/ejemplos/05/06-my_strcpy.c)

***
***
## Asignación dinámica de memoria

* Mecanismo que permite alocar memoria durante la ejecución del programa
* La memoria asignada se toma de la **heap**
* Disponemos de las siguientes funciones provistas por `stdlib.h`

```c

void *calloc(size_t nmemb, size_t size);

void *malloc(size_t size);

void *realloc(void *ptr, size_t size);

void free(void *ptr);

```

<small class="fragment">
[`man malloc`](http://man7.org/linux/man-pages/man3/malloc.3.html)
</small>

---
## Ejemplos

```c

char * p = (char *) malloc(10 * sizeof(char));

int  * q = (int *) calloc(10, sizeof(int)),
     * dyn_array = NULL,
     i;

free(p);
free (q);

for(i=1; i < 1000; i++) {
  dyn_array = realloc(dyn_array, i * sizeof(int));
}

free (dyn_array);
```

<small>
<ul class="fragment">
 <li>[Ejemplo arreglo dinamico v1](images/ejemplos/05/07-dynamic-allocation.c)</li>
 <li>[Ejemplo arreglo dinamico v2](images/ejemplos/05/08-dynamic-allocation-realloc.c)</li>
</ul>
</small>
---
## La precedencia es muy importante

Si recordamos la tabla de precedencia de operadores, podemos ver que las
primeras líneas se enumeran así

* `() [] ->`
* `! ~  ++ -- - (tipo) * & sizeof`
* `* / %`
* `+ -`
* `<< >>`
* `Continúa ...` 

<small>
**_La tabla va de mayor prioridad a menor prioridad_**
</small>
---
## ¿Qué es lo que estaría mal en el siguiente ejemplo?

```c
void some_function(** ptr) {
  ...
  *ptr[i] = some_var;
  ...
}
```

<small>
**_Considerar la tabla de prioridades_**
</small>

<small class="fragment">
[Ejemplo arreglo dinamico v2 con error](images/ejemplos/05/08-dynamic-allocation-realloc-ERROR.c)
</small>
---
## Un error común

* **No debe retornarse un arreglo definido como variable automática**
* Puede retornarse un puntero o usar una variable **static**

<small>
<ul class="fragment">
 <li>[Ejemplo error](images/ejemplos/05/09-return-array-error.c)</li>
 <li>[Ejemplo solución del error](images/ejemplos/05/09-return-array-error-SOLVED.c)</li>
</small>
***
***
## Arreglos de punteros

Como las variables de tipo punteros son como cualquier otra,
entonces podemos almacenarlas en un arreglo

### Ejemplo

* En este caso creamos un arreglo donde cada elemento es un puntero a char

```c

char *lineas[5];    /* Arreglo de 5 punteros a char */

```

* `lineas[i]` es un puntero a char

* `*lineas[i]` es el primer carácter de la línea i-ésima

---
## Ejemplos de arreglos de punteros

```c

char * palabras [ ] = {"break", "continue", "do", "if", "int"};

```

* `palabras[0]` 
  * Al igual que `*palabras`
  * Contiene *break*
* `palabras[1]` 
  * Al igual que `*(palabras +1)`
  * Contiene *continue*
* `palabras[1][2]`
  * Al igual que `*(palabras [1] +2)`
  * Al igual que `*(*(palabras +1)+2)`
  * Contiene *n*

---
## Un ejemplo

```c
#include <stdio.h>
int main()
{
  char *dias[]= { "lunes", "martes", "miercoles",
                  "jueves", "viernes", "sabado",
                  "domingo", };
  int i;
  for (i=0; i<7; i++) printf("El dia %d es %s\n",i,dias[i]);

  return 0;
}
```

<small>
[Descargar ejemplo que lee, ordena e imprime
palabras](images/ejemplos/05/10-read-words.c)
</small>

---
## Arreglos de punteros multidimensionales

Analizar qué cantidad de espacio se necesita en cada caso:

```c

  int a[10][20];

  int *b[10];

```

***
***
## Argumentos al programa principal

* La función `main` puede especificar dos parámetros
* Generalmente a estos parámetros se los llama
  * **argc**: de tipo entero
  * **argv**: de tipo char * []
* Crear un programa y verificar el valor de argv[0]

```

#include <stdio.h>

int main (int argc, char *argv[]){
  int i;
  for(i=0; i<argc; i++) printf("%s\n", argv[i]);
  return argc;
}

```
***
