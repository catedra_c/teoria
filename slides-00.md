## Seminario de Lenguajes
### Opción C
***
# La cátedra
---
## Profesores

* Christian A. Rodriguez
  * <car@info.unlp.edu.ar>
  * [@car_unlp](https://twitter.com/car_unlp)
  * https://github.com/chrodriguez
---
## Jefes de Trabajos Prácticos

* Andrés Barbieri
* Nahuel Cuesta Luengo

---
## Ayudantes
* Carina Girón
* Matías Ferrigno
* Emilia Corrons
* Valeria Soria

## Colaboradores

* Lucas Di Cunzolo
* Joaquín Gamba
***
# Contacto

* https://catedras.info.unlp.edu.ar
* <lenguajec@info.unlp.edu.ar>
***
# Horarios
---
## Horarios de Teoría

* Martes de 9:30hs a 11:00hs Aula 1-1

## Horarios de Práctica

* Martes de 8:00hs a 9:30hs Aula 1-1
* Viernes de 11:30hs a 13:30hs Aula 1-1
***
## Bibliografía

* El Lenguaje C. Kernighan & Ritchie
* C con ejemplos. Perry, Greg
* Efficient C programming. Weiss, Mark Allen
* C in a nutshell. Prinz, Peter
<p class="fragment">
<http://catalogo.info.unlp.edu.ar>
</p>
---
## Modo de aprobación
<p class="fragment" style="text-align: left;" >
La <strong>aprobación de la cursada</strong> estará dada por la aprobación de cuatro evaluaciones
en máquina presenciales:
</p>
<ul>
<li class="fragment">Si se aprueban las cuatro evaluaciones no se rinde parcial integrador</li>
<li class="fragment">Cada evaluación desaprobada se rendirá como un tema en el parcial
integrador</li>
<li class="fragment">Puede no aprobarse ninguna evaluación, siendo necesario rendir el parcial
integrador completo</li>
<li class="fragment">TP integrador individual o máximo de 2 personas, defendible en un coloquio individual</li>
</ul>

---
## Aprobación de la materia

* La **aprobación de la materia** estará dada por la aprobación de la Cursada
* Con ella se obtiene un **6 (seis)**

*Si se desea mejorar la nota se podrá realizar una extensión del trabajo 
realizado en la cursada. La nota final se promediará con
el desempeño durante la cursada*
---
