# Clase 4
---
## Temario
* El preprocesador
  * Introducción
  * Características
  * Directivas
  * Macros
  * Concatenación

---
# El preprocesador	
***
## Introducción
![El preprocesador](images/4-1-el-preprocesador.png)

---
## Introducción
* Es un elemento del lenguaje C muy utilizado, que permite dar una respuesta al principio
  de modularización y compilación separada que promueve C
* Las instrucciones normales no afectan al preprocesador, tiene comandos especiales
* Ventajas
  * Los programas son más fáciles de desarrollar
  * Son más fáciles de leer
  * Son más fáciles de modificar
  * Y el código de C es más transportable entre diferentes arquitecturas de máquinas
***
***
## Características

* Se ejecuta **antes** que la compilación.
* Realiza modificaciones en el archivo fuente.
  * Inclusión de textos.
  * Sustituciones.
  * Eliminar partes del fuente.
* Trabaja únicamente sobre los fuentes
* *No tiene en cuenta ningún aspecto sintáctico ni semántico del lenguaje*
---
## Funcionamiento
<div style="width: 45%; float: left;">
<img src="images/4-2-el-preprocesador.png" />
</div>

<div class="fragment" style="width: 45%; float: left;">
<img src="images/4-2-1-el-preprocesador.png" />
</div>

***
***
## Directivas

* Una directiva es una palabra que interpreta el preprocesador
* Comienzan con el símbolo **#** (numeral)
* Están situadas a principio de línea
* Si involucran más de una línea se incluye el carácter \\ (barra invertida) al final de la línea,
  indicando que la línea continua abajo
* No se utiliza el carácter **;** (punto y coma) al final de la línea
---
## Directivas

```c
#include
#define
#undef

#if 
#else 
#endif

#ifdef 
#ifndef 
#elif 
```
---

## #include

* Incluye el contenido del archivo indicado
* Puede usarse de las siguientes formas:

```c
#include "nombr"

#include <nombre>
```

### Ejemplo:

```c
#include <stdio.h>

#include "../parent_subdir/my_header.h"
```
---

## ¿Cómo compilamos muchos archivos?

```bash

gcc -o prog archi1.c archi2.c

```

### Las opciones

* **-c:** preprocesa y compila a código objeto
* **-o archivo:** nombre del binario definitivo
* **-Wall:** muestra todos las advertencias del compilador
  * ***Las advertencias son errores***
* **-E:** sólo realiza **preprocesamiento**
---
## #define 

```c

#define nombre textoReemplazo

```

* Sustituye **todas** las ocurrencias de ***nombre*** por ***textoReemplazo***
* Permite definir **constantes simbólicas**

```c

#define MAX 100

```

<small class="fragment highlight-blue">
En ejecución no ocupa un lugar en la memoria porque se reemplaza por una
constante
</small>
---
## #define 

* El alcance es desde donde se lo declara hasta el final del archivo fuente
  * Si el define se hace en un archivo incluido, entonces su alcance se propaga al archivo que usa el `#include`
* Las sustituciones se hacen por elementos y no por strings encerrados entre comillas (")
* **No** hay que escribir el símbolo igual (=)
* En general, el nombre se escribe en mayúsculas para diferenciarlo de las variables.
---
## Errores habituales

```c

#define MAX =100

#define MIN 10;


if (x == MAX) /* if (x == =100) */

if (x == MIN) /* if (x == 10;) */

```
---
## Errores habituales

```c

#define C A+B

D = C * C /* A+B * A+B */

```
<div class="fragment">
<h3> La forma adecuada </h3>

<pre>
<code class="c">

#define C (A+B)

</code>
</pre>
</div>

---
## #define

También permite definir **macros personalizadas**

```c

#define abs(x) ( (x) > 0 ? (x) : -(x) )

z = abs(a - 3); /* z = ( (a-3) > 0 ? (a-3) : -(a-3) ); */

```
<div class="fragment">
<h3>¿Cuál es el error de usar...?</h3>

<pre>
<code class="c">

abs(a++);

</code>
</pre>

</div>
---
## Para tener en cuenta

```c

#define swap(x,y) {int temp=x; x=y; y=temp;}

...

if (a>b)
  swap(a,b)
else

...

```

<small class="fragment highlight-red">
<strong>¿Cuál sería un posible problema?</strong>
</small>

---
## Otro ejemplo con #define

```c
#define DOS 2
#define Doble(x) (DOS*(x))
#define Cuadruple(x) (Doble(Doble(x)))

....

z=Cuadruple(A);

....

```
### Deslozado

```c

z = (Doble(Doble(A)));
z = ((DOS*(Doble(A))));
z = ((DOS*((DOS*(A)))));
z = ((2*(DOS*(A))));
z = ((2*((2*(A)))));

```


---
## ¿Cómo se comporta con recursión?

* Se efectúan los reemplazos en orden de definición y cuando llega al último reemplazo
se corta el proceso
* **No** continua en loop.

```c

#define X Y
#define Y Z
#define Z X

....

A=X;

....

/* 
A = Y;
A = Z;
A = X
*/
```

---
## Otro ejemplo recursivo

```c
#define sqrt(x) ( (x) < 0 ? 0 : sqrt(x) )

sqrt(-1);

sqrt(4);

```
<div class="fragment highlight-red">
¿Qué sucede?
</div>

---
## Macros predefinidas

* Se definen con dos guiones bajos **__**
  * `__FILE__` Es una cadena de caracteres que contiene el nombre del archivo fuente
  * `__LINE__` El número de línea actual
  * `__DATE__` fecha de compilación
  * `__TIME__` hora de la compilación
  * `__STDC__` 1, si compila ANSI C

---
## La stringificación

### Operadores de Macros

* `#`: si `x` es un parámetro de una macro, `#x` es el
parámetro actual correspondiente representado como
una cadena de caracteres.

### Ejemplo

```c

#define comoString(x) #x

comoString(3)   /* "3"      */
comoString(a b) /* "a b"    */
comoString("3") /* "\"3\""  */

```

---
## La concatenación

### Operadores de Macros

* `##`: Se aplica cuando se procesa la macro y se reemplazan los
parámetros formales por los actuales. Los dos elementos que rodean al
operador se combinan

### Ejemplo

```c

a + b##c /* a +  bc */

#define concatenar(a,b) a##b

...
char prueba[] = "Hola mundo";
printf("%s", concat(pru,eba));


```

---
## Macros o funciones

Si por alguna razón sucede que existe una macro y una función con el mismo
nombre, resolvemos el conflicto usando paréntesis en las funciones:

```c
#define cuadrado(x) (x)*(x)

double (cuadrado)(double x ){
  return x*x + 1;
}

int main() {
  printf("%.2f con macro\n", cuadrado(2.0));
  printf("%.2f con funcion\n", (cuadrado)(2.0));
  return 0;
}
```
---
## define vs const

Asumiendo los siguientes casos

```c

const int MAX = 100 + 20;

#define MAX 100 + 20

```

<small class="fragment highlight-red">
En el uso **MAX * MAX** no se obtiene el mismo resultado
</small>

---
## undef

* Permite dejar sin efecto un `define` previo

```c

#define MAX 12

..

#undef MAX

..

#define MAX 11


```
***
***
# Condicionales
---
## Compilación condicional

* El preprocesador permite incorporar o eliminar sentencias
de un código fuente según la evaluación de una expresión.

* Es posible consultar por defined

### Directivas

```c
#if ­ #else ­ #ifdef ­ #ifndef ­ #elif ­ #endif
#if defined(MAX)
...
#endif
...
#ifdef MAX
...
#endif
```

---
## #if #endif

```c

#if DEBUG
...
#endif

```
<small>
[Descargar ejemplo](images/ejemplos/04/04-if.c)
</small>

---
## ¿Cuál es el resultado?

```c
#define ARG 0
#define GB 1
#define ESP 2
#define PAIS_ACTIVO ESP

#if PAIS_ACTIVO == ARG
char moneda[] = "pesos";
#elif PAIS_ACTIVO == GB
char moneda[] = "libras";
#else
char moneda[] = "euro";
#endif
```
---
## ¿Cómo se usa en los .h?

* Para evitar inclusiones recursivas se utiliza `#ifndef` 

```c
#ifndef _HEADER
#define _HEADER

....

#ifndef MAX
#define MAX 100
#endif

...

#endif
```

<small>
[Descargar ejemplo](images/ejemplos/04/05-recursivo.zip)
</small>

***
***
## Opciones del gcc

```c
#include <stdio.h>

#ifdef LINUX

#define MENSAJE "Estamos en Linux"

#else

#define MENSAJE "NO estamos en Linux"

#endif

int main(void){
  printf(MENSAJE);
}
```

Podemos definir la macro fuera del fuente

```bash

gcc -o prog -DLINUX archi1.c

```
***
