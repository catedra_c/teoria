#include <stdio.h>

int main()
{
#ifdef ARR
  char a[10],b[10] = {'a','b',0};
  a = b;
  printf("b: %s\na: %s\n",b,a);
#else
  struct { char arr[10];} a,b = { {'a', 'b', 0} };
  a = b;
  printf("b: %s\na: %s\n",b.arr,a.arr);
#endif

  return 0;
}
