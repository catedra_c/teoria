#include "uno.h"
#include "dos.h"
#include "tres.h"

#include <stdio.h>

int main() {
  printf("El valor de UNO es %d\n", UNO);
  printf("El valor de DOS es %d\n", DOS);
  printf("El valor de TRES es %d\n", TRES);
  return 0;
}
