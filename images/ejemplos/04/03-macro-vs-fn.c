#include <stdio.h>

#define cuadrado(x) (x)*(x)

double (cuadrado)(double x ){
  return x*x + 1;
}

int main() {
  printf("%.2f con macro\n", cuadrado(2.0));
  printf("%.2f con funcion\n", (cuadrado)(2.0));
  return 0;
}
