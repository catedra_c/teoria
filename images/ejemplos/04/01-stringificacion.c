#include <stdio.h>

#define to_string(any) #any

int main() {
  int x = 2;
  char c = 'a';
  printf("%s = %d\n", to_string(x), x);
  printf("%s = %c\n", to_string(c), c);
  printf("%s = %c\n", to_string((c+2 )/4), c);
  return 0;
}
