#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void copy(FILE *in , FILE *out) {
  int c;
  while ((c=getc(in))!=EOF) fputc(c, out);
}

void myclose(FILE *in, FILE *out) {
  if ( in != stdin) fclose(in);
  if ( out != stdout) fclose(out);
}

int main(int count, char * args[]) {
  int o;
  FILE *in = stdin,
       *out = stdout;


  while ((o = getopt (count, args, "hi:o:"))!=-1) {
    switch (o) {
      case 'i':
        in = fopen(optarg, "rb");
        if (!in) {
          myclose(stdin, out);
          printf("Error abriendo %s", optarg);
          exit(EXIT_FAILURE);
        }
        break;
      case 'o':
        out = fopen(optarg, "wb");
        if (!out) {
          myclose(in, stdout);
          printf("Error abriendo %s", optarg);
          exit(EXIT_FAILURE);
        }
        break;
      case 'h':
      default:
        printf ("Uso: %s [-i <INPUT_FILE>] [-o <OUTPUT_FILE>]\n", args[0]);
        myclose(in, out);
        exit(EXIT_FAILURE);
        break;
    }
  }
  copy(in, out);
  myclose(in, out);
  exit(EXIT_SUCCESS);
}
