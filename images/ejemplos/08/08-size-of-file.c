#include <stdio.h>
#include <stdlib.h>


int main(int argc, const char * argv[]) {
  FILE *f = fopen(argv[1],"r");
  if (!f) { 
    perror("Fallo el abrir el archivo");
    exit(EXIT_FAILURE);
  }
  if (fseek(f, 0L, SEEK_END) < 0) { 
    perror("Error posicionando el archivo");
    exit(EXIT_FAILURE);
  }
  printf("Size of %s is %lu\n", argv[1], ftell(f));
  exit(EXIT_SUCCESS);
}
