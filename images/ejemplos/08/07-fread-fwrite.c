#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define STRING_SIZE 512
typedef struct {
  char first_name[STRING_SIZE];
  char last_name[STRING_SIZE];
  unsigned short document_type;
  unsigned long document_number;
} t_person;

enum t_action {CREATE, READ, NONE };

void myclose(FILE *in, FILE *out) {
  if (in) fclose(in);
  if (out) fclose(out);
}

void read_persons_into_file(FILE *out) {
  t_person p;
  int c, count;
  do
  {
    printf("\n"
        "=====================================\n"
        " Ingrese los datos de la persona\n"
        "=====================================\n");
    printf("Nombre (uno solo y sin espacios): ");
    scanf("%s", p.first_name);
    printf("Apellido (uno solo y sin espacios): ");
    scanf("%s", p.last_name);
    printf("Tipo de documento (0 => DNI | 1 => PASAPORTE): ");
    do
    {
      p.document_type = -1;
      if ((count = scanf("%hd", &p.document_type)) < 1)
      {
        getchar();
        printf("\nUd ingreso un valor que no es numerico, vuelva a cargarlo: ");
      }
      else
      {
        if (p.document_type !=0 && p.document_type!=1)
        {
          printf("\nUd ingreso un valor que no es alguno de los esperados (0 o 1), vuelva a cargarlo:" );
        }
      }
    }while (count < 1 || (p.document_type != 0 && p.document_type !=1) );
    printf("document_number: ");
    do
    {
      p.document_number = -1;
      if ((count = scanf("%lu", &p.document_number)) < 1)
      {
        getchar();
        printf("\nUd ingreso un valor que no es numerico, vuelva a cargarlo:");
      }
      else
      {
        if (p.document_number <= 0)
        {
          printf("\nUd ingreso un valor que no es positivo, vuelva a cargarlo:" );
        }
      }
    }while (count < 1 || (p.document_number <= 0));
    printf("\n"
        "-------------------------------------\n"
        "Ud ingresó:\n"
        "\tNombre: %s\n"
        "\tApellido: %s\n"
        "\tTipo documento: %d\n"
        "\tNro documento: %lu\n", p.first_name,p.last_name, p.document_type, p.document_number);
    printf("Guardar? (s/n)");
    while ((c=getchar())!='s' && c!='n');
    if (c=='s') fwrite(&p, sizeof(t_person), 1, out);
    printf("\n"
        "-------------------------------------\n");

    printf("Agregar otro? (s/n)");
    while ((c=getchar())!='s' && c!='n');
  }while(c=='s');
}

void read_persons_from_file(FILE *in)
{
  t_person p;
  while (fread(&p, sizeof(t_person), 1, in) == 1)
  {
    printf("\n"
        "-------------------------------------\n"
        "Ud ingresó:\n"
        "\tNombre: %s\n"
        "\tApellido: %s\n"
        "\tTipo documento: %d\n"
        "\tNro documento: %lu", p.first_name,p.last_name, p.document_type, p.document_number);
  }
  printf("\n"
      "-------------------------------------\n");
}

int main(int count, char *args[]) {
int o;
enum t_action action = NONE;
FILE *in = NULL,
     *out = NULL;
   while ((o = getopt (count, args, "hv:c:"))!=-1)
   {
      switch (o) 
      {
        case 'v':
          in = fopen(optarg, "rb");
          action = READ;
          if (!in)
          { 
            myclose(in, out);
            printf("Error abriendo %s", optarg);
            exit(EXIT_FAILURE);
          }
          break;
        case 'c':
          out = fopen(optarg, "wb");
          action = CREATE;
          if (!out)
          {
            myclose(in, out);
            printf("Error abriendo %s", optarg);
            exit(EXIT_FAILURE);
          }
          break;
        case 'h':
        default:
          printf (
              "Uso: %s [-v <INPUT_FILE>] | [-c <OUTPUT_FILE>].\n"
              "\n"
              "\t-v visualiza el contenido del archivo indicado\n"
              "\t-c crea un nuevo archivo solicitando los datos por teclado\n", args[0]);
          myclose(in, out);
          exit(EXIT_FAILURE);
          break;
      }
   }
   switch (action)
   {
      case NONE:
        printf("No ha indicado ninguna accion... Utilice -h para ver uso\n");
        exit(EXIT_FAILURE);
      case READ:
        read_persons_from_file(in);
        myclose(in,out);
        break;
      case CREATE:
        read_persons_into_file(out);
        myclose(in,out);
        break;
   }
   exit(EXIT_SUCCESS);
}
