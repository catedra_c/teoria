#include <mysql/mysql.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * create table sample(id int primary key auto_increment, name varchar(100), description varchar(100));
 * for i in `seq 1 100`; do echo "insert into sample(name, description) values('name $i', 'desc $i');"; done | mysql c_demo -uroot 
 */
void mysql_exit_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

int main(int argc, char **argv)
{
  MYSQL *con = mysql_init(NULL);

  if (con == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(con));
    exit(1);
  }

  if (mysql_real_connect(con, "localhost", "root", "", "c_demo", 0, NULL, 0) == NULL)
  {
    mysql_exit_with_error(con);
  }

  if (mysql_query(con, "SELECT * FROM sample"))
  {
    mysql_exit_with_error(con);
  }

  MYSQL_RES *result = mysql_store_result(con);

  if (result == NULL)
  {
    mysql_exit_with_error(con);
  }

  int num_fields = mysql_num_fields(result);

  MYSQL_ROW row;
  MYSQL_FIELD *field;

  while ((row = mysql_fetch_row(result)))
  {int i;
    for(i = 0; i < num_fields; i++)
    {
      if (i == 0)
      {
        while((field = mysql_fetch_field(result)))
        {
          printf("%s ", field->name);
        }

        printf("\n");
      }

      printf("%s  ", row[i] ? row[i] : "NULL"); 
    }
  }
  printf("\n");

  mysql_free_result(result);
  mysql_close(con);
  return 0;
}



