# Ejemplos de uso de CLI

## Jugando con videos

* Videos en ASCII art:

```
cvlc --vout caca <video>`
```

* Streaming de mi desktop:

```
cvlc screen:// :screen-fps=30 :screen-caching=100 --sout '#transcode{vcodec=mp4v,vb=4096,acodec=none,scale=1,width=800,height=600}:standard{access=http,mux=ts, dst=0.0.0.0:8080}'
```


