#include <stdio.h>
#include <string.h>

#define MAX_BUFFER 2048

char * my_strcpy(char *dst, const char *src) {
  char *aux = dst;
  while ((*dst++ = *src++));
  return aux;
}

int main(int count, const char * args[]) {
  int i;
  for (i = 0; i < count; i++)
  {
    char str[MAX_BUFFER],aux1[MAX_BUFFER];
    printf("Copiamos %s => %s(%s)\n", args[i], my_strcpy(aux1, my_strcpy(str, args[i])), str);
    printf("Verifico aux1: %s\n", aux1);
  }
  return 0;
}
