#include <stdio.h>

int main ()
{


  unsigned char x = 128;

  while ((x & 16) == 0)
  {

    printf("El valor de x es %d\n", x);

    x >>= 1;

  }

  /* El cast en el Complemento a 1 se debe a que el operador ~ convierte a
   * integer el resultado */
  printf ("El complemento a 1 de %d es %d (sin signo)\n",x, (unsigned char) ~x);
  printf ("El complemento a 1 de %d es %d\n",x, ~x);

  return 0;

}
