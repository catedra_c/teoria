#include <stdio.h>
int main()
{
  unsigned short i = 0x0804L;
  unsigned char c = i;
  float f = 3.14;

  printf("El valor del short 0x%04X\n",i);
  printf("El valor del char 0x%04X\n",c);
  printf("El valor del float %06.3f\n",f);
  i = f;
  printf("El valor del short ahora es %02d\n",i);
  return 0;
}
