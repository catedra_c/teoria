#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_pre_post_operators(void **state)
{
  int n=5,x,y;
  (void) state; /* unused */

  x=++n;
  assert_int_equal(n, 6);
  assert_int_equal(x, 6);

  y=n++;
  assert_int_equal(n, 7);
  assert_int_equal(y, 6);
}

int main()
{
  const struct CMUnitTest pre_post_operators_tests[] = {
    cmocka_unit_test(test_pre_post_operators),
  };
  return cmocka_run_group_tests(pre_post_operators_tests,
    NULL, NULL);

}
