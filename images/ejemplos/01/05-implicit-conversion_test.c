#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_implicit_conversion(void **state)
{
  int x=64, y=357;
  float f=4.95;
  char c;
  (void) state; /* unused */
  c=x;
  assert_int_equal(c,64);
  x=c;
  assert_int_equal(x,64);
  c=y;
  assert_int_equal(c,101);
  y=c;
  assert_int_equal(y,101);
  y=f;
  assert_int_equal(y,4);
}

int main()
{
  const struct CMUnitTest implicit_conversion_tests[] = {
    cmocka_unit_test(test_implicit_conversion),
  };
  return cmocka_run_group_tests(implicit_conversion_tests, 
    NULL, NULL);
}
