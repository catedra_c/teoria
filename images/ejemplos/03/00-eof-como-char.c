#include <stdio.h>

int main() {
  char c = 0XFF;
  int i = 0XFF;
  printf("%s iguales la variable c con EOF\n", c == EOF ? "Son" : "No son");
  printf("%s iguales la variable i con EOF\n", i == EOF ? "Son" : "No son");

  printf ("El valor de de c es %#010X\n", (unsigned char) c);
  printf ("El valor de de i es %#010X\n", i);
  printf ("El valor de EOF de es %#010X\n", EOF);
  /* Probar el siguiente codigo con c, luego con i de la siguiente forma:
   *    ./mi-programa < /dev/urandom 
   *
   * El problema con c es que el char 0xff es considerado EOF, mientras que con i
   * esto no sucede. Por tanto, el comportamiento adecuado del ejemplo es que el programa
   * quede en un loop infinito cuando se usa i
   * */
  /*
  while ((c=getchar()) != EOF);
  */
  while ((i=getchar()) != EOF);

  printf("Si se imprime este mensaje es porque el programa no fuciona como se espera\n");
  return 0;
}
