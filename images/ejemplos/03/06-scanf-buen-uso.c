#include <stdio.h>

int main()
{int ret,val;

  do {
    printf("\nIngrese un numero entero (-1 termina, letras se descartan): ");
    while ((ret=scanf("%d", &val)) != 1) 
    {
      fprintf(stderr, "Error leyendo un entero (scanf retorna %d). Intente nuevamente: ", ret);
      if ((ret = getchar()) == EOF) {
        fprintf(stderr, "Ingresó EOF, abortamos el programa\n");
        return 2;
      };
    }
    printf("Se leyo: %d", val);
  } while(val != -1);
  return 0;
}

