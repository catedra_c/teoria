#ifndef _BIN_TREE_H
#define _BIN_TREE_H


typedef void * t_data;
typedef int (* t_data_compare)(t_data, t_data);
typedef void (* t_data_do)(t_data);

struct t_tree_node {
  t_data data;
  struct t_tree_node * left;
  struct t_tree_node * right;
};

typedef struct t_tree_node * t_tree;

void tree_initialize(t_tree *);
void tree_add(t_tree *, t_data, t_data_compare);
void tree_inorder_do(t_tree, t_data_do);
void tree_preorder_do(t_tree, t_data_do);
void tree_postorder_do(t_tree, t_data_do);

#endif
