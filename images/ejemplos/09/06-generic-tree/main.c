#include "bin_tree.h"
#include <stdio.h>

int char_compare(void * a, void *b) {
char a_val = *((char *) a),
     b_val = *((char *) b);
    return a_val > b_val ? 1 : (a_val == b_val ? 0: -1);
}

void print_char(void * d) {
  printf("%c\n", *((char *) d));
}

int int_compare(void * a, void *b) {
int a_val = *((int *) a),
     b_val = *((int *) b);
    return a_val > b_val ? 1 : (a_val == b_val ? 0: -1);
}

void print_int(void * d) {
  printf("%d\n", *((int *) d));
}

int main() {
 char chars [] = "iueao";
/*
     i
    / \
  e     u
 /     /
 a    o
*/
 int i,ints [] = {10,1,100,3,2,5,6,9,0};
/*
       10
      /  \
    1     100
     \
      3
     / \
    2   5
         \
          6
           \
            9
*/
 t_tree t;
 t_tree t1;
 tree_initialize(&t);
 tree_initialize(&t1);
 for (i=0; chars[i]; i++) tree_add(&t, &chars[i], char_compare);
 for (i=0; ints[i]; i++) tree_add(&t1, &ints[i], int_compare);
 printf("Imprimimos en INORDER\n===================\n");
 tree_inorder_do(t, print_char);
 printf("\n");
 tree_inorder_do(t1, print_int);
 printf("Imprimimos en POSTORDER\n===================\n");
 tree_postorder_do(t, print_char);
 printf("\n");
 tree_postorder_do(t1, print_int);
 printf("Imprimimos en PREORDER\n===================\n");
 tree_preorder_do(t, print_char);
 printf("\n");
 tree_preorder_do(t1, print_int);
 // FALTA LIBERAR MEMORIA
 return 0;
}
