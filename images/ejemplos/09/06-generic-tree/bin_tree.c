#include "bin_tree.h"
#include <stdio.h>
#include <stdlib.h>


void tree_initialize(t_tree *t) {
  *t = NULL;
}

void tree_add(t_tree *t, t_data d, t_data_compare compare){
  if (!*t) // <==> *t == NULL
  { 
    *t = malloc (sizeof(struct t_tree_node));
    (*t)->data = d;
    (*t)->left = (*t)->right = NULL;
  }
  else 
  {
    if (compare((*t)->data, d)>0) tree_add(&((*t)->left), d, compare);
    else tree_add(&((*t)->right), d, compare);
  }
}


void tree_inorder_do(t_tree t, t_data_do _do) {
  if (t)
  {
    tree_inorder_do(t->left, _do);
    _do(t->data);
    tree_inorder_do(t->right, _do);
  }
}

void tree_preorder_do(t_tree t, t_data_do _do) {
  if (t)
  {
    _do(t->data);
    tree_preorder_do(t->left, _do);
    tree_preorder_do(t->right, _do);
  }
}

void tree_postorder_do(t_tree t, t_data_do _do) {
  if (t)
  {
    tree_postorder_do(t->left, _do);
    tree_postorder_do(t->right, _do);
    _do(t->data);
  }
}
