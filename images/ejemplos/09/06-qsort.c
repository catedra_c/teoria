#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int
cmpstringp(const void *p1, const void *p2)
{
    /* The actual arguments to this function are "pointers to
      pointers to char", but strcmp(3) arguments are "pointers
      to char", hence the following cast plus dereference */

    return strcmp(* (char * const *) p1, * (char * const *) p2);

    /*
     * char * const * means a pointer to a constant pointer to chars.
     * 
     * The reason this cast is performed in the code in the question the
     * following:
     *
     *    p1 and p2 are (non-const) pointers to a constant location.
     *    Let's assume the type of this location is const T.
     *
     * Now we want to cast p1 and p2 to their real types. We know that each
     * element of the array is a char *, therefore T = char *.
     * That is const T is a constant pointer to char, which is written ascw
     * char * const. 
     *
     * Since p1 and p2 are pointers to the elements of the array, they are of
     * type const T *, which is char * const *.
     */
}

int
main(int argc, char *argv[])
{
    int j;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <string>...\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    qsort(&argv[1], argc - 1, sizeof(char *), cmpstringp);

    for (j = 1; j < argc; j++)
        puts(argv[j]);
    exit(EXIT_SUCCESS);
}
