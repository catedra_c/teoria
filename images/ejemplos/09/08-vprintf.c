#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

void miprintf(const char *fmt, ...) {
  va_list ap;
  char *p;
  va_start(ap, fmt);
  for(p=(char *) fmt; *p; p++) 
  {
    if (*p=='%') 
    {
      switch(*++p)
      {
        case 'd': 
          printf("%d", (int) va_arg(ap, int));
          break;
        case 'f':
          printf("%f", (double) va_arg(ap, double));
          break;
        case 's':
          printf("%s", (char *) va_arg(ap, char*));
          break;
        default:
          printf("Error, no se entiende %%%c\n", *p);
          exit(EXIT_FAILURE);
      }
    }
    else
    {
      putchar(*p);
    }
  }
  va_end(ap);
}


int main() {
  miprintf("El %s de %d+%d=%d, y el promedio es %f\n", "resultado",5,6,5+6,(float) ((5+6)) / 2);
  miprintf("No entiende el %c");
  return 0;
}

