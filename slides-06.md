# Clase 6
---
## Temario
* Estructuras
* Definición de tipos
* Estructuras dinámicas
* Uniones

***
## Estructuras
---
## Estructuras: Introducción

* Permiten crear un tipo de datos propio.
* Agrupan variables bajo un nombre
* Permite mantener junta información relacionada
* Suele llamarse tipo de dato agregado

### Ejemplo

```c
struct etiqueta {
  tipo nombre_miembro_1;
  tipo nombre_miembro_2;
  tipo nombre_miembro_3;
}
```

---
## Variables e inicialización

En el siguiente caso, se define la estructura y declaran dos variables en la
misma línea

Luego se definen otras variables y realiza una asignación

```c

struct t_student {

  int file_number;
  char name[256];

} student_1, student_2;

struct t_student student_3;
struct t_student student_4 = { 1234, "Juan Perez" };

student_2 = student_4;

```

---
## Representación en memoria
* Cuando se declara una variable, el compilador dispone automáticamente de
suficiente memoria para almacenar todos sus miembros
* Cada variable estructura contiene su propia copia de miembros de la
estructura.
* Una estructura como la anteriormente definida se representa de la siguiente
  forma en memoria

```
        +---------------------------------+-----------------+
        |           256 bytes name        | 4 bytes file_no |
        +---------------------------------+-----------------+
```

<small>
El ejemplo es a modo ilustrativo dado que por cuestiones de alineación se
utilizan estrategias diferentes. [Ver ejemplo de
alineación](https://en.wikipedia.org/wiki/Data_structure_alignment#Typical_alignment_of_C_structs_on_x86)
</small>

---
## Variables sin nombre para el tipo

Es posible definir estructuras sin **nombre de tipo**, especificando el nombre de la
variable

```c

struct {
  int file_number;
  char name[256];
} student_1, student_2;

```

---
## Acceso a los miembros

El punto sirve para referenciar el miembro de una estructura

```c
  student_1.file_number;
  student_2.name;

  scanf("%s",student_3.name);
  scanf("%d", &student_3.file_number);
  printf("Student name %s (%d)",
          student_3.name,
          student_3.file_number);

```
---
##Estructuras anidadas

```c

  struct t_address {
    char address[256];
    char city[256];
  };

  struct t_student {
    int file_number;
    char name[256];
    float average_note;
    struct t_address address;
  }

```

---
## Arreglos de estructuras

* Las estructuras suelen incluirse en arreglos
* Primero se define la estructura y luego el arreglo:

```c
  struct t_student student_list[100];
  ...
  for(i=0; i < 100; i++) {
    printf("%d - %s",
            student_list[i].file_number,
            student_list[i].name);
  }

```
---
## Inicialización de estructuras

```c

  struct t_month {

    char * name;
    unsigned int num_days;

  } month[] = {

    {"Ninguno", 0},
    {"Enero", 31},
    {"Febrero", 28},
     .....
    {"Noviembre", 30},
    {"Diciembre", 31}
  };

```
---
## Estructuras y punteros

Los punteros a estructuras se utilizan como los punteros a cualquier otra
variable.

```c
struct t_student * ptr_student;

ptr_student = & student_1;

printf("%d - %s", (*ptr_student).file_number, (*ptr_student).name);

/* O equivalentemente */

printf("%d - %s", ptr_student->file_number, ptr_student->name);

```
---
## Estructuras con miembros dinámicos vs estáticos

Asumiendo los siguientes tipos de datos

```c
struct  t_static_student {
char name[256];
float average_note;
int file_number;
} student_1, student_2;

struct t_dinamyc_student {
char *name;
float average_note;
int file_number;
} d_student_1, d_student_2;
```
---
## ¿Qué sucede en cada caso?

```c

strcpy(student_1.name, "Juan");
student_1.average_note = 9.5;
student_1.file_number = 100;
student_2 = student_1

d_student_1.name = malloc(sizeof(char)*256);
strcpy(d_student_1.name, "Juan");
d_student_1.average_note = 9.5;
d_student_1.file_number = 100;
d_student_2 = d_student_1;

```

<small>
Analice qué sucede si modificamos `student2.name` y `d_student_2.name`
</small>

---
## Del ejemplo anterior surge algo raro

* Sabemos que no es posible asignar dos arreglos
* Pero entonces en el ejemplo anterior, algo sucede cuando asignamos `student2 =
  student1`

```c
#include <stdio.h>
int main()
{
#ifdef ARR
  char a[10],b[10] = {'a','b',0};
  a = b;
  printf("b: %s\na: %s\n",b,a);
#else
  struct { char arr[10];} a,b = { {'a', 'b', 0} };
  a = b;
  printf("b: %s\na: %s\n",b.arr,a.arr);
#endif
  return 0;
}
```
<small>
[Descargar ejemplo](images/ejemplos/06/01-show-array-ptr.c) y compilar con `-DARR`
</small>
---
## Pasaje de estructuras como argumentos

Asumiendo la siguiente estructura

```c
struct t_example { char x;  int y;  float z;  char s[10];} sample;
```

Las siguientes llamadas a función son válidas:

```c
func1(sample.x);    /* pasa el valor carácter de x */
func2(sample.y);    /* pasa el valor entero de y */
func3(&sample.z);   /* pasa la dirección de z */
func4(sample.s);    /* pasa la dirección del string s */
func5(sample.s[2]); /* pasa el valor del carácter s[2] */
func6(e1);          /* pasa la estructura completa por valor */
func7(&e1);         /* pasa la estructura completa por ref */
```
***
# Definición de tipos
---
## Typedef
* Permite definir tipos de datos a partir de tipos de datos existentes
* `typedef` **no crea un nuevo tipo** sino que crea un sinónimo para uno ya existente

```c
  typedef previous_type defined_type;
```

### Ejemplo

```
typedef int t_age; 

typedef struct {
          int file_number;
          char name[256];
        } t_student;

t_age age;
t_student student;
```
***
## Estructuras dinámicas
---
## Estructuras dinámicas

Son estructuras que se definen recursivamente
Se las utiliza para definir listas, árboles y grafos

```c
struct t_node {
  t_data data;
  struct t_node *node;
}

typedef struct t_node * t_list;
```
---
## Implementando una lista

* ¿Cuáles serían las funciones necesarias para manejar la lista?
  * Crear
  * Buscar un elemento
  * Determinar si está vacía
  * Insertar elemento
  * Eliminar elemento: *elimina la primer ocurrencia que encuentre*
  * Destruir lista
  * Operaciones para recorrer la lista

---
## Prototipos de la lista

```c
void    list_create(???);

short   list_find(???, t_data);

short   list_is_empty(???);

t_data  list_add(???, t_data);

t_data  list_delete(???, tdata);

void    list_destroy(???);

```

---
## Prototipos de la lista

```c
void    list_create(t_list *);

short   list_find(t_list, t_data);

short   list_is_empty(t_list);

t_data  list_add(t_list *, t_data);

t_data  list_delete(t_list *, tdata);

void    list_destroy(t_list *);

```

---
## Recorriendo la lista

* Los tipos de datos, suelen implementar funciones asociadas que permiten recorrer las
  estructuras sin revelar cómo fueron implementadas para simplificar su uso
* Siguiendo este patrón, crearemos lo que llamaremos iteradores
  * Un iterador conocerá la lista que recorre
  * Podrán existir diferentes iteradores para una misma lista, cada uno podrá
    estar en una posición diferente de la misma lista.
  * Los iteradores conocen la forma en que se implementa la lista para poder así
    recorrerla

---

## Prototipo de un iterador
El prototipo de un iterador sería:

```c
t_list_iterator list_iterador_create(t_list );

void list_iterator_next(t_list_iterator *);

t_data list_iterator_data(t_list_iterator );

short list_iterator_end(t_list_iterator);
```

### Ejemplo de uso

```c
i = list_iterador_create(list); 

while(! list_iterator_end(i)) {

  t_data aux = list_iterator_data(i);
  ...
  list_iterator_next( &i);
}
```

<small>
**Podría usarse un for en vez de un while**
</small>

---
## Implementaciones

* [Ver ejemplo de lista como nodos enlazados](images/ejemplos/06/02-list-linked-nodes.zip)
* [Ver ejemplo de lista implementada con arreglos](images/ejemplos/06/03-list-array.zip)
* [Ver ejemplo de árbol binario](images/ejemplos/06/04-bin-tree.zip)

***
***
## Uniones
---
## Uniones

Son variables que pueden tener distinto tipo en momentos diferentes de la
ejecución de un programa

```c

union {

  int x;

  float y;

  char z[256];

} my_var;

```
---
## Uso de union

* Se utilizan como las estructuras
* La inicialización puede realizarse sólo con el primer miembro

```c
union {
  int x;
  float y;
  char z[256];
} my_var = { 10 };
```

---
## Ejemplo

```c
struct shape {
     int type;
     double area;
     union {
       double side;
       double ratio;
       struct {
         double height;
         double width;
       } rectangle;
      } dimension;
  } my_shape;
```
***
