# Clase 3
---
## Temario
* Estructura de un programa
* Funciones y prototipos
* Entrada / Salida

***
# Estructura de un programa
---
## Estructura de un programa

* Conjunto de funciones disjuntas
* No hay anidamiento de funciones
* Función `main()`
* Prototipos
* Compilación separada
  * Archivos .c y .h

***
***
# Funciones y prototipos
---
## Funciones en C

```c

tipo_retornado nombre_función(lista de_argumentos)
{
  declaraciones_y_sentencias;
  [return expresion;]
}

```

* Si se omite el tipo_retornado se asume `int`
* Si la lista de_argumentos es vacía o la función retorna vacío usar la palabra clave `void`
* Argumentos sólo por valor

---
### Ejemplo

```c

int sumo_n (int n)
{
  int suma=0;

  for (; n>0; suma += n, n--);

  return suma;
}

```

---
### Función main

```c

#include <stdio.h>

int main()
{
  printf (“Hola Mundo”);

  return 0;
}

```

---
### Prototipo de una función

* Las funciones deben ser declaradas antes de ser utilizadas
* Permiten que el compilador lleve a cabo una fuerte comprobación de tipos
* Valida también la cantidad de parámetros
* Se incluyen antes de las funciones de nuestro programa, al principio del archivo
* Se usa la palabra clave `void` para indicar el no retorno de valor por una función y una lista de argumentos vacía
* No es necesario dar nombre a las variables en la lista de argumentos
* Colabora con la modularización y construcción de librerías.

```c

tipo nombre_funcion (tipo_p1, tipo_p2, tipo_p3)

```
***
***
# Entrada/Salida
---
## Funciones de Entrada/Salida

* C no prove palabras claves para realizar E/S
* Librería `stdio.h` de la biblioteca estándard
* Existen funciones para la E/S por *consola* y por *archivo*, aunque técnicamente son similares conceptualmente son diferentes.
---

## Entrada / Salida estándar

* Archivos estándar 
  * `STDIN`: Standard Input
  * `STDOUT`: Standard Output
  * `STDERR`: Standard Error
* También se pueden redirigir
  * `STDIN`:  se puede redirigir con `<`
  * `STDOUT`: se puede redirigir con `>`
  * `STDERR`: se puede redirigir con `2>`

---
## Entrada y Salida de Caracteres

```c

int getchar(void);

int putchar(int c);

```

* `getchar()` devuelve un entero pero se puede asignar a un char. Esto es porque
devuelve `EOF` cuando se produce un error o alcanza el fin de archivo
* `EOF` es `-1` que no es un valor válido para un char.

* putchar(int) envía a la salida estándar el caracter escrito. Envía el byte menos  significativo.

<small>
[Analizar por qué EOF no es valido para un
char](images/ejemplos/03/00-eof-como-char.c)
</small>

---

## Buffer intermedio

* La función `getchar()` toma los datos de un buffer intermedio, temportal, que se carga
con lo que ingresa el usuario desde teclado (`STDIN`)
* Lo maneja internamente el Sistema Operativo

![Buffer intermedio](images/3-1- buffer-io.png)

<small>
Analizar `stty -icanon`
</small>

---
## Buffer intermedio: ejemplo

Si un programa presenta en pantalla la siguiente pregunta:

```bash

 ¿Desea ver el informe de nuevo (presione S/N)? _

```

* Asumiendo se presiona `S`, hasta que no se presione `ENTER` *no pasan los datos ingresados del buffer al programa*
* Al programa llegan ambos caracteres: la tecla `S` y el `ENTER`

---
## Buffer intermedio

* Este buffer puede ser molesto en entornos interactivos
* `getchar()` lee de a un carácter y si ingresa más de uno quedan esperando
en la cola de entrada
* Este buffer es configurable manipulando atributos de la terminal
  * Más información en **man termios**
* Probar `stty -icanon && cat`
  * Se vuelve con `stty icanon`

---
## Ejemplo de getchar y putchar: `cat`

```c
#include <stdio.h>	

int main(void) 
{
  int c;

  printf("\nIngrese un caracter "); 
  c = getchar();

  while (c != EOF) {
    putchar(c);
    c=getchar();
  }

  return 0;
}
```
<small>
<ul>
<li><strong>Notar que</strong>
  <ul>
  <li><code>int c </code> se debe a que <code>EOF</code> es -1. Si fuese char un
valor no podría representarse
  <li><code>getchar()</code> y <code>putchar()</code> trabajan caracter a caracter.
  </ul>
</li>
</ul>
</small>

---
### Versión mejorada

```c

#include <stdio.h>  

int main(void)
{
  int c;

  while ( (c=getchar()) !=EOF) {
    putchar(c);
  }

  return 0;
}

```

---
## Descarga de ejemplos

* [Uso de getchar](images/ejemplos/03/01-getchar.c)

* [Descartando chars](images/ejemplos/03/02-getchar-descarta-buffer.c)

* [Evitando el buffer con getchar](images/ejemplos/03/02-getchar-unbuffered.c)

---
## Entrada y Salida con formato 

```c

int printf(const char *cadena_de_control, ...)

int scanf(const char *cadena_de_control, ...)

```

<small class="fragment highlight-blue">
En las entradas y salidas con formato, se pueden agregar caracteres de conversión.
</small>

---
## printf

```c

  int printf(formato, arg1, arg2, ...)

```

* Formato es un string compuesto por dos tipos de objetos:
  * Caracteres que son copiados directamente en la salida
  * Especificaciones de conversión, cada uno de los cuales causa la conversión
    e impresión de los siguientes argumentos sucesivo del printf
* *Debe existir el mismo número de caracteres de formato que de argumentos*
* **Retorna la cantidad de caracteres escritos** y -1 si se ha producido un error.
---
## Un primer ejemplo

```c

int dia = 6, a = 8;

float var_flotante = 2.52345;

char mes[]="abril";

printf("Estamos en la clase de C");

printf("Hoy es lunes %d de %s", dia, mes);

printf("Ejemplo var_flotante = %0.2f", var_flotante);

```


---

## Caracteres de conversión de printf
Cada especificación de conversión comienza con un % y termina con un carácter de conversión. 

Entre estos dos caracteres hay otros modificadores que **deben especificarse en un orden determinado**

A continuación detallamos estos modificadores
---
### Modificadores de formato
* Un # que convierte a formato alternativo
* Un 0 para llenar el padding con ceros
* Un signo - que especifica ajuste a izquierda;
* Un número que especifica un ancho mínimo del campo (si es necesario se rellena con espacios en blanco para completar este valor);
* Un punto que separa el ancho del campo de la precisión;
* Otro número, la precisión, que indica el número de dígitos después del punto decimal (si es float) o el número mínimo de dígitos para un entero, o para strings la cantidad máxima de caracteres

<small class="fragment">
[Descargar ejemplo de uso de modificadores](images/ejemplos/03/03-printf.c)
</small>

---
## Los caracteres de conversión

| Char | Tipo | Se imprime como|
|----- | ---- | ---------------|
|`d,i`| `int` | Número decimal |
|`o`| `int` | Octal sin signo (sin el cero inicial |
|`x,X`| `int` | Hexadecimal |
|`u`| `int` | Decimal sin signo |
|`c`| `int` | Caracter |
|`s`| `char *` | Cadena de caracters |
|`f`| `float` | Número flotante |

---

## Ejemplo

```c

#include<stdio.h>

int main(void)
{
  int i;

  printf("%8s %8s %8s\n", "^1", "^2", "^3");

  for (i=1; i<6; i++) printf("%8d %8d %8d\n", i, i*i, i*i*i);

  return 0;
}

```
<small>
[Descargar ejemplo](images/ejemplos/03/04-printf-tabla.c)
</small>

---
## scanf

Lee datos de `STDIN`

```c

  int scanf(const char *cadena_ctrl, …);

  /* Usada generalmente de la siguiente forma */
  scanf(tipo, &var)

```

* Donde:
  * **tipo:** tipo de dato a almacenar
  * **Ampersand (&):** indica la dirección de memoria de la variable donde se almacenará el dato. Cuando se guardan cadenas de caracteres el & se omite
  * **var:** variable para almacenar el dato

<small class="fragment highlight-red">
Retorna el número de datos a los cuales se les ha asignado un valor con éxito
</small>

---
## Ejemplo

```c
#include <stdio.h>

int main(){

  int x, y, res;

  printf("\nIngrese dos valores enteros separados por -,"
         "luego presione enter:");

  res = scanf("%d-%d", &x, &y);

  printf("\nSe leyeron: \n\t1\t=>\t%d\n\t2\t=>\t%d\n"
         "El resultado del scanf fue: %d\n", x, y, res);

  return 0;
}
```

<small>
[Descargar ejemplo 05-scanf.c](images/ejemplos/03/05-scanf.c)
</small>

---
## Buenas prácticas

```c
#include <stdio.h>

int main(){
  int val;
  do {
    printf("\nIngrese un número entero (-1 termina, "
           "letras se descartan): ");
    while (scanf("%d", &val) != 1) {
      fprintf(stderr, "Error. Intente nuevamente: ");
      getchar();
    }
    printf("Se leyó: %d", val);
  } while(val != -1);
  return 0;
}
```

<small>
[Descargar ejemplo](images/ejemplos/03/06-scanf-buen-uso.c)
</small>

---
## Consideraciones de scanf

* La cadena de control consta de 3 clases de caracteres:
  * Especificación de formato: van precedidos por % e indican el tipo de dato a leer. Tienen orden
  * Caracteres de espacio en blanco
  * Caracteres que no son espacios en blanco.
* Los espacios, las tabulaciones y los saltos de línea se usan como separadores de campo. Dependiendo de la indicación del formato, el  espacio en blanco se interpreta de diferente manera.

---
## Consideraciones

| Ejemplo  | Descripcion  |
|--- |--- |
|`scanf("%c%c%c", &a, &b, &c);` | Ingresando **"x y"** queda a con x, b con blanco y c con y|
|`scanf("%s", str);` | Ingresando "hola mundo" queda hola  |

### ¿Cómo leemos espacion con scanf?
<small class="fragment">
[Descargar ejemplo](images/ejemplos/03/07-scanf-con-espacios.c)
</small>
***
