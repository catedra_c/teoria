# Clase 8
---
## Temario
* Archivos

***
## Archivos
---
## Archivos

* En C, un archivo puede ser cualquier cosa: *desde un archivo en disco hasta una impresora*
* C provee una interfaz consistente e independiente del dispositivo al que se
  está accediendo

### Se representan internamente como

* **streams (o flujo, secuencia) de datos:**
  * Utiliza el tipo `FILE *` definido en `stdio.h`
  * Se definen: `FILE *fp;`
* **File descriptors de bajo nivel:**
  * Son enteros

*Ambas representaciones proveen funciones análogas*
---
## Operaciones sobre archivos

* Para operar con un archivo es necesario
  * **Abrirlo:** conectando con una fuente de datos (datos en disco o
    periféricos)
  * **Cerrarlo:** al finalizar la transferencia

***
***
## Operaciones con streams
---
## Abrir un stream

* Se inicializa o abre un stream a través de `fopen`
* Retorna `NULL` si hay algún error

```c

  fp = fopen(name, mode)

```

* Donde `mode` puede ser: r, w, a, r+, w+, a+
---
## Modos de fopen

* `r`: solo lectura al inicio
* `w`: trunca o crea un archivo. Se posiciona al principio
* `a`: abre un archivo para añadir datos o lo crea si no existe. Se posiciona al final
* `r+`: abre como lectura y escritura. Se posiciona al principio
* `w+`: abre como lectura y escritura, truncando el archivo si existe o
  creandolo si no existe. Se posiciona al principio
* `a+`: abre como lectura y escritura o lo crea si no existe. Se posiciona al final de archivo

<small>
En todos los casos puede agregarse una `b` al final, pero se ignora en los
sistemas POSIX. En Linux no tiene sentido, pero se aconseja su uso cuando se
debe intercambiar datos con sistemas no UNIX
</small>
---
## Cerrando un stream

Los streams se cierran utilizando `fclose`

```c
#include <stdio.h>
#include <errno.h>
#include <string.h>
extern int errno;

int main(int argc, char *argv[]) {
  FILE *fp;
  if (argc==2) {
    if ((fp = fopen(argv[1], "r"))){
      printf("El arch. %s pudo abrirse\n", argv[1]);
      fclose(fp);
    }
    else printf("ERROR al abrir %s:=> %s\n", argv[1], strerror(errno));
  }
  else printf("Debe enviar nombre de archivo como parametro\n");
  return 0;
}
```

<small>
[Descargar ejemplo](images/ejemplos/08/01-file.c)
</small>
---
## Streams estándar
* Existen los siguientes archivos que no es necesario abrir o cerrar
  * `STDIN`: solo de lectura
  * `STDERR`: solo de escritura
  * `STDOUT`: solo de escritura

---
## Lecturas desde un stream
```c
// De a bytes:
int fgetc(FILE *)

// De a lineas:
char * fgets(char *dato, int tam, FILE *fp)

// Con formato:
int fscanf(FILE *f, const char * format, ...)

// De a bloques:
size_t fread(void *dato, size_t tam, size_t cant, FILE * f)
```
---
## Escrituras desde un stream
```c
// De a bytes:
int fputc(int c, FILE *)

// De a lineas:
int fputs(const char *dato, FILE *fp)

// Con formato:
int fprintf(FILE *f, const char * format, ...)

// De a bloques:
size_t fwrite(void *dato, size_t tam, size_t cant, FILE * f)
```
---
## EOF

* Para conocer si un archivo se ha recorrido y alcanzado el fin de archivo, se
utiliza la función `feof(FILE *)`
* El uso de esta función no es compatible con todas las operaciones de lectura
  mencionadas, esto es, aunque sí se marque el EOF, esto no sucede como se
espera.

### Ejemplo

```c
  i = 0;
  while (!feof(fp)) {
    fgets(buf, sizeof(buf), fp);
    printf ("Line %4d: %s", i, buf);
    i++;
  }
```
<small class="fragment">
[Descargar ejemplo](images/ejemplos/08/02-feof-bad.c) 
| <span style="color: red">El código repite la última línea</span>
| [man fgets](http://man7.org/linux/man-pages/man3/gets.3.html)
<br />
`awk 'BEGIN {i=1}{printf("Line %4d: %s\n", i++, $0) }'`
</small>
---
## Solucionando el problema anterior

Cuando se utiliza alguna función que manipule archivos, leer claramente el man para verificar
cuál es el valor de retorno. Entonces, en el caso de `fgets` su uso correcto
sería:

```c
  i = 0;
  while (fgets(buf, sizeof(buf), fp) != NULL) {
    printf ("Linea %4d: %s", i, buf);
    i++;
  }
```

<small class="fragment">
[Descargar ejemplo fgets y feof](images/ejemplos/08/03-feof-fgets.c)
<br />
[Descargar ejemplo fgetc y EOF](images/ejemplos/08/04-feof-fgetc.c)
</small>
---
## Reusando lo aprendido con getc y putc

En las primeras clases vimos cómo implementar el comando cat, ahora podemos
implementar el comando **copy** que copia dos archivos

```c
  void copy(FILE *in , FILE *out) {
    int c;
    while ((c=getc(in))!=EOF) fputc(c, out);
  }
```

<small class="fragment">
[Descargar ejemplo copy](images/ejemplos/08/05-copy.c)
</small>
---
## Uso de fscanf y fprintf

Estas funciones trabajan con texto formateado, por lo que trabajarán con
archivos de texto y no binarios

```c
void parse(FILE *in , FILE *out) {
  int count;
  float f1,f2,f3;
  while ((count = fscanf(in,"%f,%f,%f", &f1, &f2, &f3)) ==3)
  {
    fprintf(out, "%.2f,%.2f,%.2f,%.2f\n", f1, f2, f3, f1+f2+f3);
  }
  if (!feof(in))
  {
    printf("Linea mal formada\n");
    myclose(in,out);
    exit(EXIT_FAILURE);
  }
}
```
<small class="fragment">
[Descargar ejemplo fscanf y fprintf](images/ejemplos/08/06-format.c) <em>Probar
con el [ejemplo entregado](images/ejemplos/08/06-sample.txt)</em>
</small>
---
## Uso de fread y fwrite

Estas funciones trabajan bajando la memoria tal cuál se almacena en ella, por
esto generalmente trabajarán con archivos binarios y no de texto

* Consideraciones
  * No todos los procesadores manejan la misma representación interna de tipos de
  más de un byte
  * Acordar el tipo de endian (big o little) para utilizar en el intercambio
  * Algunas soluciones:
      * [man 3 endian](https://linux.die.net/man/3/endian)
      * [man 3 byteorder](http://man7.org/linux/man-pages/man3/htons.3.html)

<small>
[Descargar ejemplo fread y fwrite](images/ejemplos/08/07-fread-fwrite.c)
</small>

---
## Uso de fseek y ftell

Estas funciones permiten posicionarse en un archivo o determinar la posición
actual del cursor en un archivo respectivamente

```c

int fseek( FILE *flujo, long desplto, int origen);
  /* Donde origen puede ser:
   *    * SEEK_SET: desde el inicio
   *    * SEEK_CUR: desde la posición actual
   *    * SEEK_END: desde el final
   * fseek(fp, 0L, SEEK_CUR)
   * fseek(fp, 0L, SEEK_SET)
   * fseek(fp, 0L, SEEK_END)
   */

long ftell( FILE *flujo);

```
<small>
[Descargar ejemplo tail](images/ejemplos/08/08-tail.c)
</small>
***
***
## Operaciones con file descriptors
Operando con funciones de bajo nivel
---
## Funciones de bajo nivel

* En vez de streams utilizan file descriptors
  * Los file descriptors son enteros
  * Las funciones de bajo nivel proveen la misma funcionalidad que las de streams
    pero además otras funciones más específicas que no tienen sentido en streams
  * Existe una equivalencia entre file descriptors y streams
---
## Los archivos estandar

* Los archivos estandar se definen en `unistd.h`
  * `int STDIN_FILENO`
     *  *valor cero*
  * `int STDOUT_FILENO`
     * *valor uno*
  * `int STDERR_FILENO`
     * *valor dos*
---
## Abriendo y cerrando archivos

Las funciones de apertura y cierre son:

```c
  int open(const char * nom, int flags, mode_t mode);
  int close(int fd)
```

<small>
[man 2 open](http://man7.org/linux/man-pages/man2/open.2.html)
<br />
[man 2 close](http://man7.org/linux/man-pages/man2/close.2.html)
</small>

### Equivalencias con STREAMS

```c
FILE * fdopen(int fd, const char * tipo);
int fileno(FILE * f);
```

<small>
[man 3 fdopen](http://linux.die.net/man/3/fdopen)
<br />
[man 3 fileno](http://linux.die.net/man/3/fileno)
</small>
---
## Uso de read y write

```c
ssize_t read(int fd, void * dato, size_t tam);

ssize_t write(int fd, const void * dato, size_t tam);
```
<small>
[man 2 read](http://man7.org/linux/man-pages/man2/read.2.html)
<br />
[man 2 write](http://man7.org/linux/man-pages/man2/write.2.html)
</small>
***
